"""A module to make a little display with a button and a text field."""

import streamlit as st
import bobrossquotes

st.write("""
    # Bob Ross Quotes

    This is a thing to show you how CERN PaaS sites work!
    """)
if st.button("Give me a quote!"):
    st.text(bobrossquotes.get_random_quote())
