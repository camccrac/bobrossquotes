"""
For making Bob Ross quotes.

Adapted from https://github.com/kz6fittycent/BobRossQuotes (MIT licensed).
"""
from os import path
import random
import textwrap

DATA_DIR = path.join(path.dirname(__file__), "data")
ASCII_ART_FILE = path.join(DATA_DIR, "ascii")
QUOTES_FILE = path.join(DATA_DIR, "quotes")


def build_bubble(quote=""):
    """Given a quote string, return an ascii-art formatted speech bubble."""
    # number of characters after which the bubble will wrap around
    wrap_limit = 50
    wrap_list = textwrap.wrap(quote, wrap_limit)

    if len(wrap_list) == 1:
        wrap_limit = len(wrap_list[0])

    for line in wrap_list:
        length_dif = wrap_limit - len(line)
        line += (' ' * length_dif) + ' |'

    bubble = ' {0}\n| {1} |'
    for i in range(0, len(wrap_list)):
        bubble += '\n| {' + str(i + 2) + '}'
    bubble += '\n| {1} |\n {0}'
    return bubble.format("-" * (wrap_limit + 2), " " * wrap_limit, *wrap_list)


def get_random_quote():
    """Print a random Bob Ross Quote."""
    text = ""
    with open(QUOTES_FILE, encoding='utf8') as quotes_file:
        text += build_bubble(random.choice(quotes_file.readlines())) + "\n"

    with open(ASCII_ART_FILE, encoding="ascii") as ascii_file:
        text += ascii_file.read() + "\n"
    return text

def main():
    """Example module usage."""
    print(get_random_quote())


if __name__ == "__main__":
    main()
